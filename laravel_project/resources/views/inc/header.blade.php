<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('./css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('./css/style.css') }}">
    
    <script src="{{ URL::asset('./js/jQuery-3.3.1.js') }}"></script> <!--  Da bi drop menu mogao da radi prvo postavljamo jquery -->
    <script src="{{ URL::asset('./js/bootstrap.min.js') }}"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    
    <title>WEB PAGE</title>
  </head>
  <body>
      <!-- content  -->
      
      
    <div class="container">
     <div class="Content">
        
        <div class="row justify-center">
          <div class="col-12">
           
              @include('inc/navbar')
              @yield('inc/content')
              
            
              
          </div>
        </div>
         
     </div>
    </div>
      
      
  </body>
</html>