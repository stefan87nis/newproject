@extends('layouts.app')
@section('inc/content')


<h1 align="center" class="my-5">Contact page</h1>


<form action="{{ url('/contact') }}" method="post">
    <div class="for-group">
        <label for="inputName">Name:</label>
        <input type="text" name="name" id="inputName" value="{{ old('name') }}"  class="form-control">
        <div>{{ $errors->first('name') }}</div>
    </div>
    
    
    <div class="for-group">
        <label for="inputEmail">Email:</label>
        <input type="text" name="email" id="inputEmail" value="{{ old('email') }}" class="form-control">
        <div>{{ $errors->first('email') }}</div>
    </div>
    
    <div class="for-group">
        <label for="inputText">Text:</label>
        <textarea type="text" name="text" id="inputText" class="form-control">{{ old('text') }}</textarea>
        <div>{{ $errors->first('text') }}</div>
    </div>
    
    @csrf
    
    <button type="submit" class="btn btn-primary">Send</button>
    
</form>


@endsection