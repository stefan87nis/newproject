<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        
            @include('inc.navbar')
            
        <main class="py-4">
            <div class="container">
                @yield('inc/content')
              
                
                  @if(session()->has('message'))
              <div class="alert alert-success" role="alert">
                {{ session('message') }}
              </div>
              @endif
              
              
                @if(session()->has('message2'))
              <div class="alert alert-danger" role="alert">
                {{ session('message2') }}
              </div>
              @endif
              
              
          </div>
        </div>
                
            </div>
        </main>
    </div>
</body>
</html>
