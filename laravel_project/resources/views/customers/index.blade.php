@extends('layouts.app')

@section('inc/content')

              <h1 class="my-5" align="center">Customers</h1>
              
              
              
              
<p><a href="{{ url('/customers.create') }}">Add customer</a></p>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
      @foreach($customers as $customer)
    <tr>
      <th scope="row">{{ $customer->id }}</th>
      <td><a href="/customers/{{ $customer->id }}">{{ $customer->name }}</a></td>
      <td>{{ $customer->email }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
              
<div class="row">
    <div class="col-12 text-center">
      {{ $customers->links() }}
    </div>
</div>   

@endsection