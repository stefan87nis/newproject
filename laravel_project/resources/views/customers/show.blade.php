@extends('layouts.app')

@section('inc/content')



 <h1 class="my-5" align="center">Customer details</h1>


 <div class="row">
     <div class="col-12">
         <p><a href="{{ url('/customers/' . $customer->id . '/edit') }}">EDIT</a></p>
         <h1>Details for {{ $customer->name }}</h1>
     </div>
 </div>
 
 <div class="row my-5">
     <div class="col-12">
         <div class="form-group">
             <a><strong>Details for: {{ $customer->name }}</strong></a>
         </div>
         <div class="form-group">
             <a><strong>Details for: {{ $customer->email }}</strong></a>
         </div>
         
         
         
        @if($customer->image)
         
            <div class="row">
                <div class="col-12">
                    <img src="{{ asset('storage/' . $customer->image ) }}" alt="" class="img-thumbnail">
                </div>
            </div>
         
        @endif
         
        
        
         <form action="/customers/{{ $customer->id }}/index" method="post">
             @method('delete')
             @csrf
         <button style="submit" class="btn btn-danger">DELETE</button>
         </form>
         
     </div>
 </div>

@endsection