@extends('layouts.app')

@section('inc/content')



              <h1 class="my-5" align="center">Add customer</h1>
              
              
<form action="{{ url('/customers') }}" method="post" enctype="multipart/form-data">
 
    @include('customers.form')
    
    
  <button type="submit" class="btn btn-primary">Add customer</button>
  
</form>              
    
              

              
@endsection