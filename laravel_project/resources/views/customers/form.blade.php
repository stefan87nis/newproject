

<div class="form-group">
 <label for="inputEmail">Email address</label>
    <input type="text" name="email" class="form-control" id="inputEmail" value="{{ old('email') ?? $customer->email }}"  placeholder="Enter email">
    
    {{ $errors->first('email') }}
    
  </div>
    <div class="form-group">
    <label for="inputName">Name</label>
    <input type="text" name="name" class="form-control" id="inputName" value="{{ old('name') ?? $customer->name }}"  placeholder="Enter name">
    
    {{ $errors->first('name') }}

  </div>

  <div class="form-group d-flex flex-column">
    <label for="inputImage">Image: </label>
    <input type="file" name="image" class="py-3">
    
    {{ $errors->first('image') }}

  </div>

@csrf