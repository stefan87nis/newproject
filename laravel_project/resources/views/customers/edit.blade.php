@extends('layouts.app')

@section('inc/content')


              <h1 class="my-5" align="center">Edit customer details</h1>
              
           
              
<div class="row">
    <div class="col-12">
        
<form action="{{ url('/customers/' . $customer->id) }}" method="post" enctype="multipart/form-data">
    @method('patch')
   
    @include('customers.form')
    
    
     <button type="submit" class="btn btn-primary">Save customer</button>
</form>       
        
    </div>
</div>
              
@endsection