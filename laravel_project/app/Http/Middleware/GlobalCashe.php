<?php

namespace App\Http\Middleware;

use Closure;

class GlobalCashe
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);
        
        $response->header('Cashe-Control', 'max-age-86400');
        
        return $response;
        // ovako preko middleware cistimo cashe
    }
}
