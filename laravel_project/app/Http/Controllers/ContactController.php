<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    
    
     public function __construct() {
        $this->middleware('auth');
        //takodje imamo opciju da u nastavku dodamo ->only() ili exept() tako biramo sta hocemo da zakljucamo tako da 
        //korisnik koji hoce da pristupi stranici mora da bude ulogovan 
        //exept je sta da moze da se vidi a only znaci koja tacno da bude zakljucana a mozemo da stavmo kao u primeru da sve bude zakljucano
    }
    
    public function create(){
        
        
        
        return view('contact.create');
    }
    
    public function store(){
        
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required|max:400'
            ]);
        
            Mail::to('stefanjevtovic87@gmail.com')->send(new ContactFormMail($data));
            return redirect('contact')->with('message', 'You message is delivered, we will contact you soon');
            //u slucaju da dodje do greske prilikom slanja emaila, ukucati u Git php artisan config:cache
    }
}
