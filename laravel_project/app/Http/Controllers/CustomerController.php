<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    
    public function __construct() {
        $this->middleware('auth');
    }
    
    
    public function index(){
    
        $customers = Customer::paginate(10);
        
        return view('customers.index', compact('customers'));
    }    

    
    public function create(Customer $customer){
        
        $customers = Customer::all();
        
        return view('customers.create', compact('customers', 'customer'));
    }
    
    public function store(){
        
        
        $customer = Customer::create($this->validateRequest());
        
        $this->storeImage($customer);
        
        event(new \App\Events\CustomerControllerEvent($customer));
        
        
       return redirect('customers')->with('message', 'Customer is added'); 
    }
    
    
    public function show(Customer $customer){
        
        return view('customers.show', compact('customer'));
    }
    
    
    public function edit(Customer $customer){
        
        $customers = Customer::all();
        
        return view('customers.edit', compact('customers', 'customer'));
    }
    
    
    public function update(Customer $customer){
        
         
        $customer->update($this->validateRequest());
        
        $this->storeImage($customer);
        
        return redirect('customers/' . $customer->id)->with('message', 'Customer is updated'); 
    }
    
    
    private function validateRequest(){
        
        return tap(request()->validate([
            
            'name' => 'required|Regex:/^[\D]+$/i|max:100',
            'email' => 'required|email|max:255',
            
            ]), function (){
            
            if(request()->hasFile('image')){
                request()->validate([
                'image' => 'file|image|max:5000',
                ]);
            }
            
        });
    }
    
    private function storeImage($customer){
        
        if(request()->has('image')){
            $customer->update([
                'image' => request()->image->store('uploads', 'public'),
            ]);
        }
    }
    
    public function destroy(Customer $customer){
        
        $customer->delete();
        
        return redirect('customers')->with('message2', 'Customers is deleted from list');
    }
}