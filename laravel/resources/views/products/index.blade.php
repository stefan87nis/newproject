@extends('inc/layout')

@section('inc/content')

              <h1 class="my-5" align="center">Proizvodi</h1>
              


<p><a href="./products.addProducts">Dodaj proizvod</a></p>


<div class="form-group">
    
    <div class="row">

@foreach( $products as $product )

    
        <div class="card" style="width: 18rem;">
            <img src="{{ $product->image }}" class="card-img-top" alt="{{ $product->name }}">
            <div class="card-body">
            <h5 class="card-title">{{ $product->name }}</h5>
            <p class="card-text">{{ $product->price }} RSD</p>
            <a href="./products/product-details/{{ $product->id }}" class="btn btn-primary">DETAILS</a>
         </div>
        </div>
        
@endforeach

    </div>    
    
</div>


<div class="row">
    <div class="col-12 text-center">
      {{ $products->links() }}
    </div>
</div>    

@endsection