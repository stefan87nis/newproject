@extends('inc/layout')

@section('inc/content')





              <h1 class="my-5" align="center">Dodaj kategoriju</h1>
              
              
    @if ($errors->any())
     @foreach ($errors->all() as $error)
     <div class="alert alert-danger" role="alert">
         <div>{{$error}}</div>
         </div>
     @endforeach
    @endif
              
    

<form action="{{ route('categories.store') }}" method="post">
  <div class="form-group">
    <label for="inputCategory">Dodaj kategoriju</label>
    <input type="text" name="title" class="form-control" id="inputCategory"  placeholder="Unesi kategoriju">
    


  <button type="submit" class="my-3 btn btn-outline-primary">DODAJ</button>
  </div>
    @csrf
</form>
              

              
@endsection