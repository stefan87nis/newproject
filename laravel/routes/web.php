<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('categories.index', 'CategoryController@index');
Route::get('categories.addCategory', 'CategoryController@create')->name('categories.create');
Route::post('categories', 'CategoryController@store')->name('categories.store');


Route::get('products.index', 'ProductController@index');
Route::get('products.addProducts', 'ProductController@create');
Route::post('products', 'ProductController@store')->name('products.store');
Route::get('products/product-details/{product}', 'ProductController@show');